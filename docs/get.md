# Get file content

**URL** : `/api/v1/get/:file?token=:user_token`

**URL Parameters** : `file=[integer]` where `file` is the ID of the file on the
server.

**Method** : `GET`

## Success Responses

**Code** : `200 OK`

**Content** : file to download

[Back](readme.md)