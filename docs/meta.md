# Show file's metadata

**URL** : `/api/v1/meta/:file?token=:user_token`

**URL Parameters** : `file=[integer]` where `file` is the ID of the file on the
server.

**Method** : `GET`

## Success Responses

**Code** : `200 OK`

**Content** :

Dates have the following format `YYYY-MM-DD hh:mm:ss` and the size is shown in bytes

```json
{
    "id": 1,
    "name": "test_file.txt",
    "created_at": "2018-01-31 16:00:00",
    "updated_at": "2018-01-31 16:00:00",
    "size": "126"
}
```

[Back](readme.md)