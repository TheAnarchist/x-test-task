# Create file from request content

**URL** : `/api/v1/create/:file?token=:user_token`

**URL Parameters** : `file=[integer]` where `file` is the ID of the file on the
server.

**Method** : `GET`

**Data constraints** Provide file content to be created.

```json
{
    "file": "[attached file]"
}
```

## Success Responses

**Code** : `200 OK`

**Content** (example):

Dates have the following format `YYYY-MM-DD hh:mm:ss` and the size is shown in bytes

```json
{
    "id": 546,
    "name": "test_file_2.txt"
}
```

[Back](readme.md)