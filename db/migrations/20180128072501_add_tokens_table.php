<?php

use Phinx\Migration\AbstractMigration;

class AddTokensTable extends AbstractMigration
{
    public function up() {
        $tokens = $this->table('user_tokens');
        $tokens->addColumn('username', 'string', ['limit' => 20])
              ->addColumn('hash', 'string', ['limit' => 40])
              ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
              ->addColumn('expired_at', 'datetime')
              ->addIndex(['username'], ['unique' => true])
              ->save();
    }

    public function down() {
        $this->dropTable('user_tokens');
    }
}
