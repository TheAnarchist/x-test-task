<?php

use Phinx\Migration\AbstractMigration;

class AddFilesTable extends AbstractMigration
{
    public function up() {
        $files = $this->table('user_files');
        $files->addColumn('username', 'string', ['limit' => 20])
              ->addColumn('name', 'string', ['limit' => 40])
              ->addColumn('real_name', 'string', ['limit' => 40])
              ->addColumn('bytes', 'integer', ['limit' => 10])
              ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
              ->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
              ->addIndex(['username', 'real_name'], ['unique' => true])
              ->save();
    }

    public function down() {
        $this->dropTable('user_files');
    }
}
