<?php

use Phinx\Seed\AbstractSeed;

class TokenSeeder extends AbstractSeed
{
    public function run() {
        if ($this->hasTable('user_tokens')) {
            $data = $this->getSeedData();
            $users = $this->table('user_tokens');
            // truncate
            $this->execute('DELETE FROM user_tokens');
            $users->insert($data)
                  ->save();
        }
    }

    /**
     * Generate data to insert in the table
     * @return array
     */
    private function getSeedData() {
        $testUser = 'test-user';
        $userWithExpiredToken = 'user-with-expired-token';

        $format = 'Y-d-m H:i:s';
        $date = new DateTime();
        $expired = clone $date;
        $date->modify('+1 year');
        $expired->modify('-2 week');

        return [
            [
                'username' => $testUser,
                'hash' => 'TST-USR',
                'expired_at' => $date->format($format),
            ],
            [
                'username' => $userWithExpiredToken,
                'hash' => 'XPR-USR',
                'expired_at' => $expired->format($format),
            ],
        ];
    }
}
