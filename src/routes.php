<?php

// Routes
$app->group('/api/v1', function () use ($app) {
    // Create file from request data
    $app->post('/create[/]', 'apiController:create')
        ->add('uploadMiddleware')
        ->add('createMiddleware');
    
    // Update file from request data
    $app->post('/update[/{id}[/]]', 'apiController:update')
        ->add('fileMiddleware')
        ->add('uploadMiddleware');

    // Get file content
    $app->get('/get[/{id}[/]]', 'apiController:get')
        ->add('fileMiddleware');

    // Get file's metadata
    $app->get('/meta[/{id}[/]]', 'apiController:meta')
        ->add('fileMiddleware');
        
    // Get list of user's files
    $app->get('/list', 'apiController:all');
})->add('apiMiddleware');
