<?php

namespace App\Controllers;

use App\Classes\FileClass;
use Illuminate\Support\Collection;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

// todo: refactor + вынести ошибки в классы

class ApiController
{
    /** @var ContainerInterface */
    protected $c = null;

    /**
     * ApiController constructor.
     *
     * @param ContainerInterface $c
     */
    public function __construct(ContainerInterface $c) {
        $this->c = $c;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return string
     * @throws \Exception
     */
    public function create(Request $request, Response $response) {
        $user = $request->getAttribute('user');
        $uploadedFiles = $request->getUploadedFiles();
        /** @var UploadedFile $file */
        $file = $uploadedFiles['file'];

        $helper = new FileClass($this->c->fs, $file);
        $filename = $helper->generateNewFilename();
        $path = $user->folder . '/' . $filename;

        if ($helper->isFileExists($filename)) {
            return $response->withStatus(409)
                ->withJson([
                    'code' => 409,
                    'message' => "File already exists",
                ]);
        };

        if (false === $helper->saveFile($path)) {
            $this->logError("Cant't create file: {$path}");
            return $response->withStatus(500)
                ->withJson([
                    'code' => 500,
                    'message' => "Can't create file",
                ]);
        }

        $record = $helper->createFileRecord($user->username, $filename);
        if (false === $record) {
            $json = json_encode($file);
            $this->logError("Cant't create record for file: {$json}");
            return $response->withStatus(500)
                ->withJson([
                    'code' => 500,
                    'message' => "Can't create record for file",
                ]);
        }

        return $response->withJson([
            'id' => $record->id,
            'name' => $record->real_name,
        ]);
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return string
     * @throws \Exception
     */
    public function update($request, $response) {
        $user = $request->getAttribute('user');
        $oldFile = $request->getAttribute('file');;
        $uploadedFiles = $request->getUploadedFiles();
        /** @var UploadedFile $file */
        $file = $uploadedFiles['file'];
        $helper = new FileClass($this->c->fs, $file);
        $filename = $helper->generateNewFilename();
        $oldPath = $user->folder . '/' . $oldFile->name;
        $path = $user->folder . '/' . $filename;

        if (false === $helper->replaceFile($path, $oldPath)) {
            $this->logError("Cant't update file: {$path}");
            return $response->withStatus(500)
                ->withJson([
                    'code' => 500,
                    'message' => "Can't update file",
                ]);
        }

        $success = $helper->updateFileRecord($oldFile->id, $filename);
        if (false === $success) {
            $json = json_encode($file);
            $this->logError("Cant't update record for file: {$json}");
            return $response->withStatus(500)
                ->withJson([
                    'code' => 500,
                    'message' => "Can't update record for file",
                ]);
        }

        return $response->withJson([
            'id' => $oldFile->id,
            'name' => $file->getClientFilename(),
        ]);
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     */
    public function get($request, $response) {
        $user = $request->getAttribute('user');
        $file = $request->getAttribute('file');

        $path = $user->folder . '/' . $file->name;
        if (false === $this->c->fs->has($path)) {
            return $response->withStatus(500)
                ->withJson([
                    'code' => 500,
                    'message' => "File not found",
                ]);
        }

        $content = $this->c->fs->read($path);
        $body = $response->getBody();
        $body->write($content);

        $size = $this->c->fs->getSize($path);
        $response = $this->setHeadersForDownloading($response, $file->real_name, $size);

        return $response->withBody($body);
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return string
     */
    public function meta(Request $request, Response $response) {
        $file = $request->getAttribute('file');
        $file->size = $file->bytes;

        $file->name = $file->real_name;
        unset($file->username);
        unset($file->real_name);
        unset($file->bytes);

        return $response->withJson($file);
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return string
     */
    public function all(Request $request, Response $response) {
        $user = $request->getAttribute('user');
        /** @var Collection $files */
        $files = $this->c->db->table('user_files')
            ->select(['id', 'real_name as name'])
            ->where('username', '=', $user->username)
            ->get();

        // another way to fetch list of files (w/o) IDs
        // $files = $this->c->fs->listContents($user->folder);

        return $response->withJson($files->all());
    }

    /**
     * @param Response $response
     * @param string   $filename
     * @param integer  $size
     *
     * @return Response
     */
    private function setHeadersForDownloading($response, $filename, $size) {
        $response = $response->withHeader('Content-Description', 'File Transfer');
        $response = $response->withHeader('Content-Type', 'application/octet-stream');
        $response = $response->withHeader('Content-Disposition', 'attachment; filename=' . $filename);
        $response = $response->withHeader('Content-Transfer-Encoding', 'binary');
        $response = $response->withHeader('Expires', '0');
        $response = $response->withHeader('Cache-Control', 'must-revalidate');
        $response = $response->withHeader('Pragma', 'public');
        return $response->withHeader('Content-Length',  $size);
    }

    /**
     * @param string $error
     */
    private function logError($error) {
        $this->c->logger->error($error);
    }
}
