<?php

namespace App\Middleware;

use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;
use App\Classes\FileClass;


class UploadMiddleware
{
    /** @var ContainerInterface */
    protected $c = null;

    /**
     * AuthMiddleware constructor.
     *
     * @param ContainerInterface $c
     */
    public function __construct($c) {
        $this->c = $c;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param          $next
     *
     * @return string
     */
    public function __invoke($request, $response, $next) {
        $user = $request->getAttribute('user');
        $uploadedFiles = $request->getUploadedFiles();
        if (false === isset($uploadedFiles['file'])) {
            return $response->withStatus(404)
                ->withJson([
                    'code' => 404,
                    'message' => 'File not found in request',
                ]);
        }

        /** @var UploadedFile $file */
        $file = $uploadedFiles['file'];
        $stream = $file->getStream();
        if (false === $stream->isReadable()) {
            $this->logError("Directory {$user->folder} isn't writable");
            return $response->withStatus(500)
                ->withJson([
                    'code' => 500,
                    'message' => "Can't save file",
                ]);
        }
        
        return $next($request, $response);
    }
}