<?php

namespace App\Middleware;

use Illuminate\Support\Collection;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class FileMiddleware
{
    /** @var ContainerInterface */
    protected $c = null;

    /**
     * AuthMiddleware constructor.
     *
     * @param ContainerInterface $c
     */
    public function __construct($c) {
        $this->c = $c;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param          $next
     *
     * @return string
     */
    public function __invoke($request, $response, $next) {
        $args = $request->getAttribute('routeInfo')[2];
        if (false === isset($args['id'])) {
            return $response->withStatus(404)
                ->withJson([
                    'code' => 404,
                    'message' => 'Empty file id',
                ]);
        }

        $fileId = $args['id'];
        /** @var Collection $files */
        $files = $this->getFileRecord($fileId);
        if ($files->isEmpty()) {
            return $response->withStatus(404)
                ->withJson([
                    'code' => 404,
                    'message' => 'File not found',
                ]);
        }

        $file = $files->first();
        // File not found in filesystem but has a record in 'user_files' table
        if (false === $this->isSetFileInFileSystem($request, $file)) {
            $this->c->logger->error('File not found: ' . json_encode($file));
            return $response->withStatus(500)
                ->withJson([
                    'code' => 500,
                    'message' => 'File not found',
                ]);
        }

        // pass the file info to the next
        $newRequest = $request->withAttribute('file', $file);

        return $next($newRequest, $response);
    }

    /**
     * @param $id
     *
     * @return Collection
     */
    private function getFileRecord($id) {
        return $this->c->db->table('user_files')
            ->where('id', '=', $id)
            ->get();
    }

    /**
     * @param Request $request
     * @param         $file
     *
     * @return bool
     */
    private function isSetFileInFileSystem($request, $file) {
        $user = $request->getAttribute('user');
        $path = $user->folder . '/' . $file->name;

        return $this->c->fs->has($path);
    }
}