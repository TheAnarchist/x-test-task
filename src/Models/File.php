<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Files
 */
class File extends Model
{
    protected $table = 'user_files';
    protected $fillable = [
        'username',
        'name',
        'real_name',
        'bytes',
        'created_at',
        'updated_at'
    ];
}
