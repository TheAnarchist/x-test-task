<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Token
 */
class Token extends Model
{
    protected $table = 'user_tokens';
    protected $fillable = ['username', 'hash', 'created_at', 'expired_at'];
}
