<?php

namespace Tests\Functional;

class FileMiddlewareTest extends BaseTestCase
{
    protected $baseUrl = '/api/v1/meta';
    protected $token = 'TST-USR';
    protected $fileId = '1';

    /**/
    public function testEmptyFile() {
        $url = $this->baseUrl . '?token=' . $this->token;
        $response = $this->runApp('GET', $url);
        $this->assertEquals(404, $response->getStatusCode());

        $json = (string) $response->getBody();
        $this->assertJson($json);

        // check structure of error and error message
        $arr = json_decode($json, true);
        $this->assertArrayHasKey('message', $arr);
        $this->assertEquals('Empty file id', $arr['message']);
    }

    /**/
    public function testWrongFile() {
        $url = $this->baseUrl . '/wrong-file?token=' . $this->token;
        $response = $this->runApp('GET', $url);
        $this->assertEquals(404, $response->getStatusCode());

        $json = (string) $response->getBody();
        $this->assertJson($json);

        // check structure of error and error message
        $arr = json_decode($json, true);
        $this->assertArrayHasKey('message', $arr);
        $this->assertEquals('File not found', $arr['message']);
    }

    /**/
    public function testCorrectFile() {
        $url = $this->baseUrl . '/' . $this->fileId . '?token=' . $this->token;
        $response = $this->runApp('GET', $url);
        $this->assertEquals(200, $response->getStatusCode());

        $json = (string) $response->getBody();
        $this->assertJson($json);
    }
}
