<?php

namespace Tests\Functional;

/**
 * Class ApiTest
 * todo: загрузка, обновление и получение файла
 */
class ApiTest extends BaseTestCase
{
    protected $baseUrl = '/api/v1/';
    protected $token = 'TST-USR';
    protected $fileId = '1';

    /**/
    public function testList() {
        $url = $this->baseUrl . 'list?token=' . $this->token;
        $response = $this->runApp('GET', $url);
        $this->assertEquals(200, $response->getStatusCode());

        $json = (string) $response->getBody();
        $this->assertJson($json);

        // check structure of json
        $arr = json_decode($json, true);
        $first = $arr[0];
        $this->assertArrayHasKey('id', $first);
        $this->assertArrayHasKey('name', $first);
    }

    public function testMeta() {
        $url = $this->baseUrl . 'meta/' . $this->fileId . '?token=' . $this->token;
        $response = $this->runApp('GET', $url);
        $this->assertEquals(200, $response->getStatusCode());

        $json = (string) $response->getBody();
        $this->assertJson($json);

        // check structure of json
        $arr = json_decode($json, true);
        $this->assertArrayHasKey('id', $arr);
        $this->assertArrayHasKey('name', $arr);
        $this->assertArrayHasKey('created_at', $arr);
        $this->assertArrayHasKey('updated_at', $arr);
        $this->assertArrayHasKey('size', $arr);

        // check date format
        $format = 'Y-m-d H:i:s';
        $dCreated = \DateTime::createFromFormat($format, $arr['created_at']);
        $this->assertInstanceOf('Datetime', $dCreated);
        $dLastModified = \DateTime::createFromFormat($format, $arr['updated_at']);
        $this->assertInstanceOf('Datetime', $dLastModified);
    }

    /**/
    public function testGet() {
        $url = $this->baseUrl . 'get/' . $this->fileId . '?token=' . $this->token;
        $response = $this->runApp('GET', $url);
        $this->assertEquals(200, $response->getStatusCode());

        $headers = $response->getHeaders();
        $this->assertArrayHasKey('Content-Type', $headers);
        $this->assertEquals($headers['Content-Type'][0], 'application/octet-stream');
        $this->assertArrayHasKey('Content-Disposition', $headers);
        $this->assertArrayHasKey('Content-Length', $headers);
    }
}
